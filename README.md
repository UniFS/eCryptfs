# ZFS - Best!
Code
- https://www.phoronix.com/scan.php?page=news_item&px=ZFS-On-Linux-0.8-Released

Fied on NixOS
- https://www.phoronix.com/scan.php?page=news_item&px=NixOS-Linux-5.0-ZFS-FPU-Drop

Guide
- https://datacenteroverlords.com/2017/12/17/zfs-and-linux-and-encryption-part-1/

Architecture
- https://blog.heckel.io/2017/01/08/zfs-encryption-openzfs-zfs-on-linux/


# eCryptFS
about:
- https://en.wikipedia.org/wiki/ECryptfs

Guide:
- https://wiki.archlinux.org/index.php/ECryptfs

Limitation:
- Ceph https://tracker.ceph.com/projects/ceph/wiki/Cephfs_encryption_support

# Btrfs
Not quite ready!
- https://btrfs.wiki.kernel.org/index.php/FAQ#Does_btrfs_support_encryption.3F
- https://www.phoronix.com/scan.php?page=news_item&px=Btrfs-AES-Encryption-2019

# other
- https://en.wikipedia.org/wiki/List_of_cryptographic_file_systems